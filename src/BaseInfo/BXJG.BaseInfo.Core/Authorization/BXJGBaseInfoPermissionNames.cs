﻿namespace BXJG.BaseInfo.Authorization
{
    public static class BXJGBaseInfoPermissionNames
    {
        public const string BXJGBaseInfo = "BXJGBaseInfo";

        //{codegenerator}

        #region 行政区
        public const string BXJGBaseInfoAdministrative = "BXJGBaseInfoAdministrative";
        public const string BXJGBaseInfoAdministrativeCreate = "BXJGBaseInfoAdministrativeCreate";
        public const string BXJGBaseInfoAdministrativeUpdate = "BXJGBaseInfoAdministrativeUpdate";
        public const string BXJGBaseInfoAdministrativeDelete = "BXJGBaseInfoAdministrativeDelete";
        #endregion
    }
}
